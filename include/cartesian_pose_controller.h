#pragma once

#include <array>

#include <Eigen/Core>

#include <franka/control_types.h>
#include <franka/duration.h>
#include <franka/robot.h>
#include <franka/robot_state.h>

class CartesianPoseController {
  public:
    CartesianPoseController(double speed_factor, double goal_tolerance_, const std::array<double, 6> pose_goal, const franka::Model* model);
    franka::CartesianVelocities operator()(const franka::RobotState& robot_state, franka::Duration period);

  private:
    using Vector6d = Eigen::Matrix<double, 6, 1, Eigen::ColMajor>;

    const Vector6d pose_goal_;
    const franka::Model* model_;
    const double speed_factor_;
    const double goal_tolerance_;

    const double stop_tolerance_ = 0.001;
    const double filter_tau_ = 0.001;
    const double vel_cap_ = 0.5;

    double curr_x_;
    double curr_y_;
    double curr_z_;

    double filtered_vel_x_;
    double filtered_vel_y_;
    double filtered_vel_z_;

    bool goal_reached_();
    bool slowed_down_();
};
