#include <iostream>
#include <sstream>
#include <cmath>
#include <sys/time.h>

#include <Eigen/Geometry>
#include <Eigen/Dense>
#include <Eigen/Core>

#include <franka/exception.h>
#include <franka/robot.h>
#include <franka/model.h>

#include "ros/ros.h"
#include "ros/console.h"

#include "tf/tf.h"
#include "tf2_geometry_msgs/tf2_geometry_msgs.h"
#include "tf2_ros/transform_listener.h"

#include "std_msgs/Float64MultiArray.h"
#include "std_msgs/String.h"
#include "geometry_msgs/PoseStamped.h"

#include "joint_pose_controller.h"
#include "cartesian_pose_controller.h"

//=============================================
// PARAMS

const double POSE_CONTROL_SPEED_JOINT         = 0.5;
const double POSE_CONTROL_SPEED_CARTESIAN     = 1.0;
const double POSE_CONTROL_TOLERANCE_CARTESIAN = 0.01;
const double MAIN_LOOP_SLEEP_TIME             = 0.01;

//=============================================
// TYPES

enum ControlMode {
  CONTROL_IDLE,
  POSE_CONTROL_JOINT,
  POSE_CONTROL_CARTESIAN,
};

//=============================================
// MINIMAL STATE VARIABLES

std::array<double, 6> target_pose_cartesian;
std::array<double, 7> target_pose_joint;

ControlMode control_mode = CONTROL_IDLE;

tf2_ros::Buffer transform_buffer;

//=============================================
// FUNCTIONS

void target_callback_joint(const std_msgs::Float64MultiArray::ConstPtr& msg) {
  if (msg->data.size() != 7) { std::cerr << "[ERROR] target_callback_joint(): msg->data.size() = " << msg->data.size() << " != 7" << std::endl; return; }
  for (int i = 0; i < 7; ++i) target_pose_joint[i] = msg->data[i];
  control_mode = POSE_CONTROL_JOINT;
}

//---------------------------------------------

void target_callback_cartesian(const geometry_msgs::PoseStamped::ConstPtr& msg) {
  geometry_msgs::PoseStamped commanded_pose = *msg;
  geometry_msgs::PoseStamped transformed_pose;
  geometry_msgs::TransformStamped base_to_pose;

  try {
    base_to_pose = transform_buffer.lookupTransform("panda_link0", msg->header.frame_id, msg->header.stamp, ros::Duration(1.0));
  } catch (tf::LookupException& e) {
    ROS_ERROR_STREAM(e.what());
    return;
  }

  tf2::doTransform(commanded_pose, transformed_pose, base_to_pose);

  // position
  double xx = transformed_pose.pose.position.x;
  double yy = transformed_pose.pose.position.y;
  double zz = transformed_pose.pose.position.z;

  // orientation
  double r, p, y;
  tf::Quaternion quat(transformed_pose.pose.orientation.x, transformed_pose.pose.orientation.y, transformed_pose.pose.orientation.z, transformed_pose.pose.orientation.w);
  tf::Matrix3x3 rotation_matrix(quat);
  rotation_matrix.getRPY(r, p, y);

  ROS_INFO("target_callback_cartesian(): target_pose = {%lf %lf %lf %lf %lf %lf %lf}",
           transformed_pose.pose.   position.x,
           transformed_pose.pose.   position.y,
           transformed_pose.pose.   position.z,
           transformed_pose.pose.orientation.x,
           transformed_pose.pose.orientation.y,
           transformed_pose.pose.orientation.z,
           transformed_pose.pose.orientation.w);

  target_pose_cartesian = std::array<double, 6>({{xx, yy, zz, r, p, y}});
  control_mode = POSE_CONTROL_CARTESIAN;
}

//---------------------------------------------

void publish_control_mode(const ros::Publisher& pub) {
  std::stringstream ss;
  switch (control_mode) {
    case CONTROL_IDLE:           ss << "CONTROL_IDLE";           break;
    case POSE_CONTROL_JOINT:     ss << "POSE_CONTROL_JOINT";     break;
    case POSE_CONTROL_CARTESIAN: ss << "POSE_CONTROL_CARTESIAN"; break;
  }
  std_msgs::String msg;
  msg.data = ss.str();
  pub.publish(msg);
}

//---------------------------------------------

int execute_control(franka::Robot& robot, franka::Model& model, ros::Publisher& control_state_pub) {
  try {
    if (control_mode == POSE_CONTROL_JOINT) {
      JointPoseController joint_pose_controller(POSE_CONTROL_SPEED_JOINT, target_pose_joint);
      publish_control_mode(control_state_pub);
      robot.control(joint_pose_controller);
    }
    else if (control_mode == POSE_CONTROL_CARTESIAN) {
      CartesianPoseController cartesian_pose_controller(POSE_CONTROL_SPEED_CARTESIAN, POSE_CONTROL_TOLERANCE_CARTESIAN, target_pose_cartesian, &model);
      publish_control_mode(control_state_pub);
      robot.control(cartesian_pose_controller);
    }
  } catch (const franka::ControlException& e) {
    std::cout << "Caught franka::ControlException: " << e.what() << std::endl;
    robot.automaticErrorRecovery();
    return EXIT_FAILURE;
  } catch (const franka::Exception& e) {
    std::cout << "Caught franka::Exception: " << e.what() << std::endl;
    robot.automaticErrorRecovery();
    return EXIT_FAILURE;
  }
  return EXIT_SUCCESS;
}

//=============================================
// MAIN

int main(int argc, char** argv) {
  if (argc != 2) { std::cerr << "Usage: " << argv[0] << " <robot-hostname (172.16.0.2?)>" << std::endl; return -1; }

  ros::init(argc, argv, "minipanda");
  ros::NodeHandle node;

  tf2_ros::TransformListener transform_listener(transform_buffer);

  franka::Robot robot(argv[1]);
  franka::Model model(robot.loadModel());

  robot.setCollisionBehavior(
      {{20.0, 20.0, 18.0, 18.0, 16.0, 14.0, 12.0}}, {{20.0, 20.0, 18.0, 18.0, 16.0, 14.0, 12.0}},
      {{20.0, 20.0, 18.0, 18.0, 16.0, 14.0, 12.0}}, {{20.0, 20.0, 18.0, 18.0, 16.0, 14.0, 12.0}},
      {{20.0, 20.0, 20.0, 25.0, 25.0, 25.0}},       {{20.0, 20.0, 20.0, 25.0, 25.0, 25.0}},
      {{20.0, 20.0, 20.0, 25.0, 25.0, 25.0}},       {{20.0, 20.0, 20.0, 25.0, 25.0, 25.0}});
  robot.setJointImpedance({{3000, 3000, 3000, 2500, 2500, 2000, 2000}});
  robot.setCartesianImpedance({{3000, 3000, 3000, 300, 300, 300}});

  ros::Subscriber pose_sub_joint     = node.subscribe("pose_target_joint",     1, target_callback_joint);
  ros::Subscriber pose_sub_cartesian = node.subscribe("pose_target_cartesian", 1, target_callback_cartesian);
  ros::Publisher  control_state_pub  = node.advertise<std_msgs::String>("control_mode", 100);

  ROS_INFO_STREAM("Waiting for control targets.");

  while (ros::ok()) {
    ros::spinOnce();

    // execute controller and check for success
    if (execute_control(robot, model, control_state_pub) != EXIT_SUCCESS) {
      std::cerr << "Controller failed. Exiting..." << std::endl;
      return EXIT_FAILURE;
    }

    control_mode = CONTROL_IDLE;
    publish_control_mode(control_state_pub);

    ros::Duration(MAIN_LOOP_SLEEP_TIME).sleep();
  }
  return EXIT_SUCCESS;
}
