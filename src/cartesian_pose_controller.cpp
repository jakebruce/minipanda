#include <algorithm>
#include <array>
#include <cmath>

#include <franka/exception.h>
#include <franka/robot.h>

#include "ros/ros.h"

#include "cartesian_pose_controller.h"

//============================================
// CARTESIAN POSE CONTROLLER CLASS

CartesianPoseController::CartesianPoseController(double speed_factor, double goal_tolerance, const std::array<double, 6> pose_goal, const franka::Model* model)
  : pose_goal_(pose_goal.data()), speed_factor_(speed_factor), goal_tolerance_(goal_tolerance), model_(model) {
  curr_x_ = 0;
  curr_y_ = 0;
  curr_z_ = 0;
  filtered_vel_x_ = 0;
  filtered_vel_y_ = 0;
  filtered_vel_z_ = 0;
}

//--------------------------------------------

bool CartesianPoseController::goal_reached_() {
  if (fabs(curr_x_ - pose_goal_[0]) > goal_tolerance_) return false;
  if (fabs(curr_y_ - pose_goal_[1]) > goal_tolerance_) return false;
  if (fabs(curr_z_ - pose_goal_[2]) > goal_tolerance_) return false;
  return true;
}

//--------------------------------------------


bool CartesianPoseController::slowed_down_() {
  if (fabs(filtered_vel_x_) > stop_tolerance_) return false;
  if (fabs(filtered_vel_y_) > stop_tolerance_) return false;
  if (fabs(filtered_vel_z_) > stop_tolerance_) return false;
  return true;
}

//--------------------------------------------

franka::CartesianVelocities CartesianPoseController::operator()(const franka::RobotState& robot_state, franka::Duration period) {
  // current state
  curr_x_ = robot_state.O_T_EE[12];
  curr_y_ = robot_state.O_T_EE[13];
  curr_z_ = robot_state.O_T_EE[14];

  // goal state
  double goal_x = pose_goal_[0];
  double goal_y = pose_goal_[1];
  double goal_z = pose_goal_[2];

  // vector to goal
  double diff_x = goal_x - curr_x_;
  double diff_y = goal_y - curr_y_;
  double diff_z = goal_z - curr_z_;

  // velocities
  double dx = std::min(std::max(speed_factor_ * diff_x, -vel_cap_), vel_cap_);
  double dy = std::min(std::max(speed_factor_ * diff_y, -vel_cap_), vel_cap_);
  double dz = std::min(std::max(speed_factor_ * diff_z, -vel_cap_), vel_cap_);

  if (goal_reached_() or not ros::ok()) {
    dx = 0.0;
    dy = 0.0;
    dz = 0.0;
  }

  filtered_vel_x_ = filter_tau_ * dx + (1-filter_tau_) * filtered_vel_x_;
  filtered_vel_y_ = filter_tau_ * dy + (1-filter_tau_) * filtered_vel_y_;
  filtered_vel_z_ = filter_tau_ * dz + (1-filter_tau_) * filtered_vel_z_;

  std::array<double, 6> velocities = {{filtered_vel_x_, filtered_vel_y_, filtered_vel_z_, 0, 0, 0}};
  franka::CartesianVelocities output(velocities);

  if (goal_reached_() && slowed_down_()) return franka::MotionFinished(output);
  else return output;
}
